import socketio
from flask import Flask, render_template
# from binance.websockets import BinanceSocketManager
# from binance.client import Client
# from binance.enums import *
async_mode = None

sio = socketio.Server(logger=True, engineio_logger=True,async_mode=async_mode)
app = Flask(__name__)
app.wsgi_app = socketio.Middleware(sio, app.wsgi_app)

@sio.on('connect')
def connect(sid, environ):
    sio.enter_room(sid, "room_test")
    # IS EMITING. OK  <========================================================== #3
    sio.emit("markets", ["CONNECT"], room="room_test", namespace="/")


def callback(msg):
    print("msg : ",msg["s"])
    # NOT EMITING. CLIENT RECEIVE NOTHING  <===================================== #2
    sio.emit("markets", ["CALLBACK"], room="room_test", namespace="/")

if __name__ == '__main__':
    # client = Client("key",
    #                 "secretKey")
    #
    # # The binance websocket start in another thread <============================ #1
    # bm = BinanceSocketManager(client)
    # bm.start_kline_socket('BNBBTC', callback, interval=KLINE_INTERVAL_1MINUTE)
    # bm.start()

    # app.wsgi_app = socketio.Middleware(sio, app.wsgi_app)
    app.run(threaded=True)