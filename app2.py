from flask import Flask, render_template
from flask_socketio import Namespace, SocketIO, emit
from time import sleep
from concurrent.futures import ThreadPoolExecutor

# DOCS https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ThreadPoolExecutor
executor = ThreadPoolExecutor(2)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

from flask_socketio import Namespace, emit

# class MyCustomNamespace(Namespace):
#     def on_connect(self):
#         pass
#
#     def on_disconnect(self):
#         pass
#
#     def some_long_task1(self):
#         # sleep(10)
#         emit('my response', str('Process 1 Done'))
#         print("SSS")
#
#     def some_long_task2(self, arg1, arg2):
#         # sleep(5)
#         print("DDD")
#         self.emit('my response', str('Process 2 Done'))
#         print("SSS")
#         # sleep(5)
#         # print("Task #2 is done!")
#
#     def on_my_event(self, data):
#         executor.submit(self.some_long_task1)
#         executor.submit(self.some_long_task2)
#         emit('my response', data)
#
# socketio.on_namespace(MyCustomNamespace('/'))


@socketio.on('connect')
def connect():
    print("connect ")

@socketio.on('message')
def handle_message(message):
    print('received message: ' + message)

@socketio.on('json')
def handle_json(json):
    print('received json: ' + str(json))

@socketio.on('my_event')
def handle_my_custom_event(json):
    # emit('my response', str(json), broadcast=True)
    print('received json: ' + str(json))
    emit('my response', str('hhh'))

    def some_long_task1():
        # sleep(10)
        emit('my_response', str('Process 1 Done'))

    def some_long_task2(arg1, arg2):
        # sleep(5)
        emit('my_response', str('Process 2 Done'))
        # sleep(5)
        # print("Task #2 is done!")

    executor.submit(some_long_task1)
    executor.submit(some_long_task2, 'hello', 123)


if __name__ == '__main__':
    app.run(threaded=True)